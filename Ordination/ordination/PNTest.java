package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

public class PNTest {

	private Laegemiddel paracetamol = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

	@Test
	public void testPNTC1() {
		PN pn = new PN(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), paracetamol, 1);
		assertEquals(1, pn.getAntalEnheder(), 0.0001);
		assertEquals(paracetamol, pn.getLaegemiddel());
		assertEquals(LocalDate.of(2021, 2, 1), pn.getStartDen());
		assertEquals(LocalDate.of(2021, 3, 1), pn.getSlutDen());
		assertEquals(0, pn.getAntalGangeGivet());
	}

	@Test
	public void testPNTC2() {
		PN pn = new PN(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), paracetamol, 110);
		assertEquals(110, pn.getAntalEnheder(), 0.0001);
		assertEquals(paracetamol, pn.getLaegemiddel());
		assertEquals(LocalDate.of(2021, 2, 1), pn.getStartDen());
		assertEquals(LocalDate.of(2021, 3, 1), pn.getSlutDen());
		assertEquals(0, pn.getAntalGangeGivet());
	}

	@Test
	public void testGivDosisTC1() {
		PN pn = new PN(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), paracetamol, 1);
		int antal = pn.getAntalGangeGivet();
		int datoSize = pn.getDatoerForDosis().size();
		assertEquals(true, pn.givDosis(LocalDate.of(2021, 2, 1)));
		assertEquals(antal + 1, pn.getAntalGangeGivet(), 0.001);
		assertEquals(datoSize + 1, pn.getDatoerForDosis().size(), 0.001);

	}

	@Test
	public void testGivDosisTC2() {
		PN pn = new PN(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), paracetamol, 1);
		int antal = pn.getAntalGangeGivet();
		int datoSize = pn.getDatoerForDosis().size();
		assertEquals(true, pn.givDosis(LocalDate.of(2021, 2, 11)));
		assertEquals(antal + 1, pn.getAntalGangeGivet(), 0.001);
		assertEquals(datoSize + 1, pn.getDatoerForDosis().size(), 0.001);
	}

	@Test
	public void testGivDosisTC3() {
		PN pn = new PN(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), paracetamol, 1);
		int antal = pn.getAntalGangeGivet();
		int datoSize = pn.getDatoerForDosis().size();
		assertEquals(true, pn.givDosis(LocalDate.of(2021, 3, 1)));
		assertEquals(antal + 1, pn.getAntalGangeGivet(), 0.001);
		assertEquals(datoSize + 1, pn.getDatoerForDosis().size(), 0.001);
	}

	@Test
	public void testGivDosisTC4() {
		PN pn = new PN(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), paracetamol, 1);
		assertEquals(false, pn.givDosis(LocalDate.of(2021, 1, 31)));
	}

	@Test
	public void testGivDosisTC5() {
		PN pn = new PN(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), paracetamol, 1);
		assertEquals(false, pn.givDosis(LocalDate.of(2021, 3, 2)));
	}

	@Test
	public void testDoegnDosisTC1() {
		PN pn = new PN(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), paracetamol, 2);
		assertEquals(0, pn.doegnDosis(), 0.01);
	}

	@Test
	public void testDoegnDosisTC2() {
		PN pn = new PN(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), paracetamol, 2);
		pn.givDosis(LocalDate.of(2021, 2, 1));
		pn.givDosis(LocalDate.of(2021, 2, 5));
		pn.givDosis(LocalDate.of(2021, 3, 1));
		pn.givDosis(LocalDate.of(2021, 2, 6));
		pn.givDosis(LocalDate.of(2021, 2, 8));
		assertEquals(0.34, pn.doegnDosis(), 0.01);
	}

	@Test
	public void testDoegnDosisTC3() {
		PN pn = new PN(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), paracetamol, 2);
		pn.givDosis(LocalDate.of(2021, 2, 4));
		pn.givDosis(LocalDate.of(2021, 2, 5));
		pn.givDosis(LocalDate.of(2021, 2, 6));
		pn.givDosis(LocalDate.of(2021, 2, 8));
		pn.givDosis(LocalDate.of(2021, 2, 25));
		assertEquals(0.45, pn.doegnDosis(), 0.01);
	}

	@Test
	public void testDoegnDosisTC4() {
		PN pn = new PN(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), paracetamol, 1.3);
		pn.givDosis(LocalDate.of(2021, 2, 4));
		pn.givDosis(LocalDate.of(2021, 2, 5));
		pn.givDosis(LocalDate.of(2021, 2, 6));
		pn.givDosis(LocalDate.of(2021, 2, 8));
		pn.givDosis(LocalDate.of(2021, 2, 25));
		assertEquals(0.30, pn.doegnDosis(), 0.01);
	}

	@Test
	public void testSamletDosisTC1() {
		PN pn = new PN(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), paracetamol, 2);
		assertEquals(0, pn.samletDosis(), 0.01);
	}

	@Test
	public void testSamletDosisTC2() {
		PN pn = new PN(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), paracetamol, 2);
		pn.givDosis(LocalDate.of(2021, 2, 4));
		pn.givDosis(LocalDate.of(2021, 2, 5));
		pn.givDosis(LocalDate.of(2021, 2, 6));
		pn.givDosis(LocalDate.of(2021, 2, 8));
		pn.givDosis(LocalDate.of(2021, 2, 25));
		assertEquals(10, pn.samletDosis(), 0.01);
	}

	@Test
	public void testSamletDosisTC3() {
		PN pn = new PN(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), paracetamol, 1.3);
		pn.givDosis(LocalDate.of(2021, 2, 4));
		pn.givDosis(LocalDate.of(2021, 2, 5));
		pn.givDosis(LocalDate.of(2021, 2, 6));
		pn.givDosis(LocalDate.of(2021, 2, 8));
		pn.givDosis(LocalDate.of(2021, 2, 25));
		assertEquals(6.5, pn.samletDosis(), 0.01);
	}
}
