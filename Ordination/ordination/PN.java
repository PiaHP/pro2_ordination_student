package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;

public class PN extends Ordination {

	private double antalEnheder;
	private int antalGangeGivet = 0;
	private ArrayList<LocalDate> datoerForDosis = new ArrayList<>();

	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double antalEnheder) {
		super(startDen, slutDen);
		super.setLaegemiddel(laegemiddel);
		this.antalEnheder = antalEnheder;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		return antalGangeGivet;
	}

	public ArrayList<LocalDate> getDatoerForDosis() {
		return new ArrayList<>(datoerForDosis);
	}

	/**
	 * Registrerer at der er givet en dosis på dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes.
	 * Returnerer false og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		if (givesDen.isBefore(getSlutDen()) && givesDen.isAfter(getStartDen()) || givesDen.isEqual(getSlutDen())
				|| givesDen.isEqual(getStartDen())) {
			antalGangeGivet++;
			datoerForDosis.add(givesDen);
			return true;
		} else {
			return false;
		}
	}

	public double samletDosis() {
		double samletDosis = datoerForDosis.size() * antalEnheder;
		return samletDosis;
	}

	public double doegnDosis() {
		if (!datoerForDosis.isEmpty()) {
			Collections.sort(datoerForDosis);
			LocalDate sidste = datoerForDosis.get(datoerForDosis.size() - 1);
			LocalDate første = datoerForDosis.get(0);
			int antalDage = (int) (ChronoUnit.DAYS.between(første, sidste) + 1);
			double doegnDosis = (double) ((getAntalGangeGivet() * getAntalEnheder()) / antalDage);
			return doegnDosis;
		} else {
			return 0;
		}
	}

	@Override
	public String getType() {
		String PN = "PN dosis";
		return PN;
	}
}