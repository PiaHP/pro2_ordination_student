package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

public class DagligSkaevTest {
	private Laegemiddel l = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

	@Test
	public final void testDagligSkaev1() {
		LocalTime[] klokkeslet = {};
		double[] antal = {};

		DagligSkaev ds = new DagligSkaev(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), l, klokkeslet, antal);

		assertEquals(l, ds.getLaegemiddel());
		assertEquals(LocalDate.of(2021, 2, 1), ds.getStartDen());
		assertEquals(LocalDate.of(2021, 3, 1), ds.getSlutDen());
		assertEquals(0, ds.getDoser().size(), 0.01);

	}

	@Test
	public final void testDagligSkaev2() {
		LocalTime[] klokkeslet = { LocalTime.of(8, 0), LocalTime.of(10, 0), LocalTime.of(14, 0), LocalTime.of(17, 0),
				LocalTime.of(0, 0) };
		double[] antal = { 1, 4, 2, 3, 110 };

		DagligSkaev ds = new DagligSkaev(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), l, klokkeslet, antal);

		assertEquals(l, ds.getLaegemiddel());
		assertEquals(LocalDate.of(2021, 2, 1), ds.getStartDen());
		assertEquals(LocalDate.of(2021, 3, 1), ds.getSlutDen());
		assertEquals(5, ds.getDoser().size(), 0.01);
	}

	@Test
	public final void testDoegnDosis1() {
		LocalTime[] klokkeslet = { LocalTime.of(8, 0), LocalTime.of(10, 0), LocalTime.of(14, 0), LocalTime.of(17, 0),
				LocalTime.of(0, 0) };
		double[] antal = { 1, 4, 2, 3.4, 110 };

		DagligSkaev ds = new DagligSkaev(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), l, klokkeslet, antal);

		assertEquals(120.4, ds.doegnDosis(), 0.01);
	}

	@Test
	public final void testDoegnDosis2() {
		LocalTime[] klokkeslet = { LocalTime.of(8, 0) };
		double[] antal = { 5 };

		DagligSkaev ds = new DagligSkaev(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), l, klokkeslet, antal);

		assertEquals(5, ds.doegnDosis(), 0.01);
	}

	@Test
	public final void testSamletDosis1() {
		LocalTime[] klokkeslet = { LocalTime.of(8, 0), LocalTime.of(10, 0), LocalTime.of(14, 0), LocalTime.of(17, 0),
				LocalTime.of(0, 0) };
		double[] antal = { 1, 4, 2, 3, 110 };

		DagligSkaev ds = new DagligSkaev(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 2, 1), l, klokkeslet, antal);

		assertEquals(120, ds.samletDosis(), 0.01);
	}

	@Test
	public final void testSamletDosis2() {
		LocalTime[] klokkeslet = { LocalTime.of(8, 0), LocalTime.of(10, 0), LocalTime.of(14, 0), LocalTime.of(17, 0),
				LocalTime.of(0, 0) };
		double[] antal = { 1, 4, 2, 3, 110 };

		DagligSkaev ds = new DagligSkaev(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 2, 10), l, klokkeslet, antal);

		assertEquals(1200, ds.samletDosis(), 0.01);
	}

	@Test
	public final void testSamletDosis3() {
		LocalTime[] klokkeslet = { LocalTime.of(8, 0), LocalTime.of(10, 0), LocalTime.of(14, 0), LocalTime.of(17, 0),
				LocalTime.of(0, 0) };
		double[] antal = { 1, 4, 2, 3, 110 };

		DagligSkaev ds = new DagligSkaev(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 5, 5), l, klokkeslet, antal);

		assertEquals(11280, ds.samletDosis(), 0.01);
	}

	@Test
	public final void testSamletDosis4() {
		LocalTime[] klokkeslet = { LocalTime.of(8, 0), LocalTime.of(10, 0), LocalTime.of(14, 0), LocalTime.of(17, 0),
				LocalTime.of(0, 0) };
		double[] antal = { 1, 4, 0.5, 3, 110 };

		DagligSkaev ds = new DagligSkaev(LocalDate.of(2021, 2, 1), LocalDate.of(2022, 2, 1), l, klokkeslet, antal);

		assertEquals(43371, ds.samletDosis(), 0.01);
	}

}