package ordination;

import static org.junit.Assert.*;
import java.time.LocalDate;
import org.junit.Test;

public class DagligFastTest {

	private Laegemiddel l = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

	@Test
	public void testDagligFastNoValues() {

		DagligFast TC1 = new DagligFast(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), l, 0, 0, 0, 0);
		assertEquals(null, TC1.getDoser()[0]);
		assertEquals(null, TC1.getDoser()[1]);
		assertEquals(null, TC1.getDoser()[2]);
		assertEquals(null, TC1.getDoser()[3]);
		assertEquals(LocalDate.of(2021, 2, 1), TC1.getStartDen());
		assertEquals(LocalDate.of(2021, 3, 1), TC1.getSlutDen());
	}

	@Test
	public void testDagligFast() {

		DagligFast dfTC2 = new DagligFast(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), l, 110, 110, 110, 110);
		assertEquals(110, dfTC2.getDoser()[0].getAntal(), 0.01);
		assertEquals(110, dfTC2.getDoser()[1].getAntal(), 0.01);
		assertEquals(110, dfTC2.getDoser()[2].getAntal(), 0.01);
		assertEquals(110, dfTC2.getDoser()[3].getAntal(), 0.01);
		assertEquals(LocalDate.of(2021, 2, 1), dfTC2.getStartDen());
		assertEquals(LocalDate.of(2021, 3, 1), dfTC2.getSlutDen());
	}

	@Test
	public void testDoegnDosisNoValues() {
		DagligFast dfTC1 = new DagligFast(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), l, 0, 0, 0, 0);
		assertEquals(0, dfTC1.doegnDosis(), 0.01);
	}

	@Test
	public void testDoegnDosis() {
		DagligFast dfTC2 = new DagligFast(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), l, 0, 1.5, 2, 300);
		assertEquals(303.5, dfTC2.doegnDosis(), 0.01);
	}

	@Test
	public void testSamletDosis1() {
		DagligFast dfTC1 = new DagligFast(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 2, 1), l, 1.5, 0, 2, 1);
		assertEquals(4.5, dfTC1.samletDosis(), 0.01);
	}

	@Test
	public void testSamletDosis2() {
		DagligFast dfTC2 = new DagligFast(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 2, 10), l, 1.5, 0, 2, 1);
		assertEquals(45, dfTC2.samletDosis(), 0.01);
	}

	@Test
	public void testSamletDosis3() {
		DagligFast dfTC3 = new DagligFast(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 5, 5), l, 1.5, 0, 2, 1);
		assertEquals(423, dfTC3.samletDosis(), 0.01);
	}

	@Test
	public void testSamletDosis4() {
		DagligFast dfTC4 = new DagligFast(LocalDate.of(2021, 2, 1), LocalDate.of(2022, 2, 1), l, 1.5, 0, 2, 1);
		assertEquals(1647, dfTC4.samletDosis(), 0.01);
	}

}
