package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private final ArrayList<Dosis> doser = new ArrayList<>();;

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, LocalTime[] klokkeSlet,
			double[] antalEnheder) {
		super(startDen, slutDen);
		super.setLaegemiddel(laegemiddel);

		for (int i = 0; i < klokkeSlet.length; i++) {
			opretDosis(klokkeSlet[i], antalEnheder[i]);
		}
	}

	public ArrayList<Dosis> getDoser() {
		return doser;
	}

	public void opretDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		doser.add(dosis);
	}

	@Override
	public double samletDosis() {
		double samletDosis = this.doegnDosis() * super.antalDage();
		return samletDosis;
	}

	@Override
	public double doegnDosis() {
		double doegnDosis = 0;
		for (Dosis dosis : doser) {
			doegnDosis += dosis.getAntal();
		}
		return doegnDosis;
	}

	@Override
	public String getType() {
		String dagligSkaev = "Daglig skæv dosis";
		return dagligSkaev;
	}
}
