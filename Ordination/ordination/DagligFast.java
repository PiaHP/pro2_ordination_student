package ordination;

import java.time.*;

public class DagligFast extends Ordination {

	private final Dosis[] doser = new Dosis[4];

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double morgenAntal,
			double middagAntal, double aftenAntal, double natAntal) {
		super(startDen, slutDen);
		super.setLaegemiddel(laegemiddel);
		if (morgenAntal != 0)
			doser[0] = new Dosis(LocalTime.of(8, 0), morgenAntal);
		if (middagAntal != 0)
			doser[1] = new Dosis(LocalTime.of(12, 0), middagAntal);
		if (aftenAntal != 0)
			doser[2] = new Dosis(LocalTime.of(18, 0), aftenAntal);
		if (natAntal != 0)
			doser[3] = new Dosis(LocalTime.of(0, 0), natAntal);
	}

	public Dosis[] getDoser() {
		return doser;
	}

	@Override
	public double samletDosis() {
		double samletDosis = this.doegnDosis() * super.antalDage();
		return samletDosis;
	}

	@Override
	public double doegnDosis() {
		double doegnDosis = 0;
		for (int i = 0; i < doser.length; i++) {
			if (doser[i] != null)
				doegnDosis += doser[i].getAntal();
		}
		return doegnDosis;
	}

	@Override
	public String getType() {
		String dagligFast = "Daglig fast dosis";
		return dagligFast;
	}
}
