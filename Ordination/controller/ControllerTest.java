package controller;

import static org.junit.Assert.*;
import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.PN;
import ordination.Patient;

public class ControllerTest {

	private Patient ulla = new Patient("011064-1522", "Ulla Nielsen", 59.9);
	private Laegemiddel l = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
	private Laegemiddel l1 = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
	private Laegemiddel l2 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
	private Controller c = Controller.getTestController();

	@Test
	public void testOpretPNOrdinationTC1() {
		Ordination pn = c.opretPNOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), ulla, l, 1);

		assertEquals(pn, ulla.getOrdinationer().get(ulla.getOrdinationer().size() - 1));
		assertEquals(l, pn.getLaegemiddel());
	}

	@Test
	public void testOpretPNOrdinationTC2() {
		Ordination pn = c.opretPNOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), ulla, l, 110);

		assertEquals(pn, ulla.getOrdinationer().get(ulla.getOrdinationer().size() - 1));
		assertEquals(l, pn.getLaegemiddel());
	}

	@Test
	public void testOpretPNOrdinationTC3() {
		try {
			c.opretPNOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 1, 1), ulla, l, 1);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Slut dato er før start dato");
		}
	}

	@Test
	public void testOpretDagligFastOrdinationTC1() {
		Ordination df = c.opretDagligFastOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), ulla, l, 0, 0,
				0, 0);

		assertEquals(df, ulla.getOrdinationer().get(ulla.getOrdinationer().size() - 1));
		assertEquals(l, df.getLaegemiddel());
	}

	@Test
	public void testOpretDagligFastOrdinationTC2() {
		Ordination df = c.opretDagligFastOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), ulla, l, 110,
				110, 110, 110);

		assertEquals(df, ulla.getOrdinationer().get(ulla.getOrdinationer().size() - 1));
		assertEquals(l, df.getLaegemiddel());
	}

	@Test
	public void testOpretDagligFastOrdinationTC3() {
		try {
			c.opretDagligFastOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 1, 1), ulla, l, 110, 110, 110,
					110);
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Slut dato er før start dato");
		}
	}

	@Test
	public void testOpretDagligSkaevOrdinationTC1() {
		LocalTime[] klokkeslet = {};
		double[] antal = {};
		Ordination ds = c.opretDagligSkaevOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), ulla, l,
				klokkeslet, antal);

		assertEquals(ds, ulla.getOrdinationer().get(ulla.getOrdinationer().size() - 1));
		assertEquals(l, ds.getLaegemiddel());
	}

	@Test
	public void testOpretDagligSkaevOrdinationTC2() {
		LocalTime[] klokkeslet = { LocalTime.of(8, 0), LocalTime.of(10, 0), LocalTime.of(14, 0), LocalTime.of(17, 0),
				LocalTime.of(0, 0) };
		double[] antal = { 1, 4, 2, 3, 110 };
		Ordination ds = c.opretDagligSkaevOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), ulla, l,
				klokkeslet, antal);

		assertEquals(ds, ulla.getOrdinationer().get(ulla.getOrdinationer().size() - 1));
		assertEquals(l, ds.getLaegemiddel());
	}

	@Test
	public void testOpretDagligSkaevOrdinationTC3() {
		try {
			LocalTime[] klokkeslet = { LocalTime.of(8, 0), LocalTime.of(10, 0), LocalTime.of(14, 0),
					LocalTime.of(17, 0), LocalTime.of(0, 0) };
			double[] antal = { 1, 4, 2, 3, 110 };
			c.opretDagligSkaevOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 1, 1), ulla, l, klokkeslet,
					antal);

		} catch (Exception e) {
			assertEquals(e.getMessage(), "Slut dato er før start dato");
		}
	}

	@Test
	public void testOpretDagligSkaevOrdinationTC4() {
		try {
			LocalTime[] klokkeslet = { LocalTime.of(8, 0), LocalTime.of(10, 0), LocalTime.of(14, 0),
					LocalTime.of(17, 0), LocalTime.of(0, 0) };
			double[] antal = { 1, 4, 2, 3 };
			c.opretDagligSkaevOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), ulla, l, klokkeslet,
					antal);

		} catch (Exception e) {
			assertEquals(e.getMessage(), "Antal enheder og klokkeslet stemmer ikke overens");
		}
	}

	@Test
	public void testOrdinationPNAnvendtTC1() {
		PN pn = c.opretPNOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), ulla, l, 2);
		int antal = pn.getAntalGangeGivet();

		c.ordinationPNAnvendt(pn, LocalDate.of(2021, 2, 3));
		assertEquals(antal + 1, pn.getAntalGangeGivet());
	}

	@Test
	public void testOrdinationPNAnvendtTC2() {
		try {
			PN pn = c.opretPNOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), ulla, l, 2);
			c.ordinationPNAnvendt(pn, LocalDate.of(2021, 1, 1));
		} catch (Exception e) {
			assertEquals(e.getMessage(), "Dato er ikke inden for gyldighedsperiode");
		}
	}

	@Test
	public void testAnbefaletDosisPrDoegnTC1() {
		Patient kasper = new Patient("260918-5327", "Kasper Nielsen", 20.0);

		assertEquals(1, c.anbefaletDosisPrDoegn(kasper, l), 0.001);
	}

	@Test
	public void testAnbefaletDosisPrDoegnTC2() {
		Patient henriette = new Patient("010516-1020", "Henriette Nielsen", 25.0);

		assertEquals(1.5, c.anbefaletDosisPrDoegn(henriette, l), 0.001);
	}

	@Test
	public void testAnbefaletDosisPrDoegnTC3() {
		Patient peter = new Patient("121084-1311", "Peter Nielsen", 120.0);

		assertEquals(1.5, c.anbefaletDosisPrDoegn(peter, l), 0.001);
	}

	@Test
	public void testAnbefaletDosisPrDoegnTC4() {
		Patient marie = new Patient("210387-1628", "Marie Nielsen", 130.0);

		assertEquals(2, c.anbefaletDosisPrDoegn(marie, l), 0.001);
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddelTC1() {
		new Patient("011064-1529", "Paul Nielsen", 60.9);
		Patient henriette = c.opretPatient("010516-1020", "Henriette Nielsen", 25.0);
		Patient peter = c.opretPatient("121084-1311", "Peter Nielsen", 120.0);
		Patient marie = c.opretPatient("210387-1628", "Marie Nielsen", 130.0);
		Patient kasper = c.opretPatient("260918-5327", "Kasper Nielsen", 20.0);

		c.opretDagligFastOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 2, 1), kasper, l, 2, 10, 0, 1);
		c.opretDagligFastOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 2, 1), kasper, l1, 2, 10, 0, 1);
		c.opretDagligFastOrdination(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 4, 1), kasper, l, 2, 10, 0, 1);

		LocalTime[] klokkeslet = { LocalTime.of(10, 0), LocalTime.of(14, 0), LocalTime.of(17, 0) };
		double[] antal = { 2, 3, 1 };

		c.opretDagligSkaevOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), henriette, l, klokkeslet,
				antal);
		c.opretPNOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), henriette, l1, 4);

		c.opretPNOrdination(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 5, 1), peter, l1, 3);

		c.opretDagligFastOrdination(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 5, 1), marie, l1, 2, 0, 0, 2);

		c.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 5), LocalDate.of(2021, 6, 7), marie, l, klokkeslet, antal);

		assertEquals(3, c.antalOrdinationerPrVægtPrLægemiddel(20, 120, l1));
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddelTC2() {
		new Patient("011064-1529", "Paul Nielsen", 60.9);
		Patient henriette = c.opretPatient("010516-1020", "Henriette Nielsen", 25.0);
		Patient peter = c.opretPatient("121084-1311", "Peter Nielsen", 120.0);
		Patient marie = c.opretPatient("210387-1628", "Marie Nielsen", 130.0);
		Patient kasper = c.opretPatient("260918-5327", "Kasper Nielsen", 20.0);

		c.opretDagligFastOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 2, 1), kasper, l, 2, 10, 0, 1);
		c.opretDagligFastOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 2, 1), kasper, l1, 2, 10, 0, 1);
		c.opretDagligFastOrdination(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 4, 1), kasper, l, 2, 10, 0, 1);

		LocalTime[] klokkeslet = { LocalTime.of(10, 0), LocalTime.of(14, 0), LocalTime.of(17, 0) };
		double[] antal = { 2, 3, 1 };

		c.opretDagligSkaevOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), henriette, l, klokkeslet,
				antal);
		c.opretPNOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), henriette, l1, 4);

		c.opretPNOrdination(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 5, 1), peter, l1, 3);

		c.opretDagligFastOrdination(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 5, 1), marie, l1, 2, 0, 0, 2);

		c.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 5), LocalDate.of(2021, 6, 7), marie, l, klokkeslet, antal);

		assertEquals(1, c.antalOrdinationerPrVægtPrLægemiddel(100, 135, l));
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddelTC3() {
		new Patient("011064-1529", "Paul Nielsen", 60.9);
		Patient henriette = c.opretPatient("010516-1020", "Henriette Nielsen", 25.0);
		Patient peter = c.opretPatient("121084-1311", "Peter Nielsen", 120.0);
		Patient marie = c.opretPatient("210387-1628", "Marie Nielsen", 130.0);
		Patient kasper = c.opretPatient("260918-5327", "Kasper Nielsen", 20.0);

		c.opretDagligFastOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 2, 1), kasper, l, 2, 10, 0, 1);
		c.opretDagligFastOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 2, 1), kasper, l1, 2, 10, 0, 1);
		c.opretDagligFastOrdination(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 4, 1), kasper, l, 2, 10, 0, 1);

		LocalTime[] klokkeslet = { LocalTime.of(10, 0), LocalTime.of(14, 0), LocalTime.of(17, 0) };
		double[] antal = { 2, 3, 1 };

		c.opretDagligSkaevOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), henriette, l, klokkeslet,
				antal);
		c.opretPNOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), henriette, l1, 4);

		c.opretPNOrdination(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 5, 1), peter, l1, 3);

		c.opretDagligFastOrdination(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 5, 1), marie, l1, 2, 0, 0, 2);

		c.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 5), LocalDate.of(2021, 6, 7), marie, l, klokkeslet, antal);

		assertEquals(0, c.antalOrdinationerPrVægtPrLægemiddel(30, 100, l1));
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddelTC4() {
		new Patient("011064-1529", "Paul Nielsen", 60.9);
		Patient henriette = c.opretPatient("010516-1020", "Henriette Nielsen", 25.0);
		Patient peter = c.opretPatient("121084-1311", "Peter Nielsen", 120.0);
		Patient marie = c.opretPatient("210387-1628", "Marie Nielsen", 130.0);
		Patient kasper = c.opretPatient("260918-5327", "Kasper Nielsen", 20.0);

		c.opretDagligFastOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 2, 1), kasper, l, 2, 10, 0, 1);
		c.opretDagligFastOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 2, 1), kasper, l1, 2, 10, 0, 1);
		c.opretDagligFastOrdination(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 4, 1), kasper, l, 2, 10, 0, 1);

		LocalTime[] klokkeslet = { LocalTime.of(10, 0), LocalTime.of(14, 0), LocalTime.of(17, 0) };
		double[] antal = { 2, 3, 1 };

		c.opretDagligSkaevOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), henriette, l, klokkeslet,
				antal);
		c.opretPNOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), henriette, l1, 4);

		c.opretPNOrdination(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 5, 1), peter, l1, 3);

		c.opretDagligFastOrdination(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 5, 1), marie, l1, 2, 0, 0, 2);

		c.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 5), LocalDate.of(2021, 6, 7), marie, l, klokkeslet, antal);

		assertEquals(4, c.antalOrdinationerPrVægtPrLægemiddel(150, 5, l));
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddelTC5() {
		new Patient("011064-1529", "Paul Nielsen", 60.9);
		Patient henriette = c.opretPatient("010516-1020", "Henriette Nielsen", 25.0);
		Patient peter = c.opretPatient("121084-1311", "Peter Nielsen", 120.0);
		Patient marie = c.opretPatient("210387-1628", "Marie Nielsen", 130.0);
		Patient kasper = c.opretPatient("260918-5327", "Kasper Nielsen", 20.0);

		c.opretDagligFastOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 2, 1), kasper, l, 2, 10, 0, 1);
		c.opretDagligFastOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 2, 1), kasper, l1, 2, 10, 0, 1);
		c.opretDagligFastOrdination(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 4, 1), kasper, l, 2, 10, 0, 1);

		LocalTime[] klokkeslet = { LocalTime.of(10, 0), LocalTime.of(14, 0), LocalTime.of(17, 0) };
		double[] antal = { 2, 3, 1 };

		c.opretDagligSkaevOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), henriette, l, klokkeslet,
				antal);
		c.opretPNOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 3, 1), henriette, l1, 4);

		c.opretPNOrdination(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 5, 1), peter, l1, 3);

		c.opretDagligFastOrdination(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 5, 1), marie, l1, 2, 0, 0, 2);

		c.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 5), LocalDate.of(2021, 6, 7), marie, l, klokkeslet, antal);

		assertEquals(0, c.antalOrdinationerPrVægtPrLægemiddel(130, 20, l2));
	}
}
